#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QTabWidget>
#include "PointDefinitions.h"
#include <QFileInfo>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QPushButton *mPbLoadPCDFile,*mPbLoadLasCloud,*mPbLoadLasTraj,*mPbComputeUpsampling,*mPbComputeDist2TrajFilter;
    QTabWidget *mTabMainWidget;
    pcl::PointCloud<pcl::PointXYZ>::Ptr mPCDCloud;
    pcl::PointCloud<pcl::PointXYZ>::Ptr mUpsamplingCloud;

    pcl::PointCloud<PointMMS>::Ptr mLasCloud;
    pcl::PointCloud<PointTraj>::Ptr mTrajCloud;

    int mTranslationX;
    int mTranslationY;


    QFileInfo *mLasFileInfo,*mPcdFileInfo;
private slots:
    void on_pbLoadPCDFile_Trigered();
    void on_pbLoadLasCLoud_Trigered();
    void on_pbLoadLasTraj_Trigered();
    void on_pbComputeUpsampling_Trigered();
    void on_pbComputeDist2TrajFilter_Trigered();
};

#endif // MAINWINDOW_H
