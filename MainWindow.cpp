#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QVBoxLayout>
#include <QFileDialog>
#include <QFileInfo>
#include <QTextStream>
#include <QMessageBox>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/surface/mls.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/surface/poisson.h>
#include <pcl/geometry/mesh_io.h>

#include <pcl/kdtree/kdtree_flann.h>

#include <liblas/liblas.hpp>
#include <fstream>

//LAS FILTERS

//Definición de clases para el filtrado de nubes de puntos las.

//LAST return
class LastReturnFilter: public liblas::FilterI //Definimos una clase que herede de "liblas::FilterI"
{
public:
    LastReturnFilter(); //Constructor
    bool filter(const liblas::Point& point); //Método público que evalua si el punto cumple la condición del filtro

private:
    LastReturnFilter(LastReturnFilter const& other);
    LastReturnFilter& operator=(LastReturnFilter const& rhs);
};
//Implementación del constructor: "eInclusion" establece que si se cumple la condición el punto es incluido
//Implementación del constructor: "eExclusion" establece que si se cumple la condición el punto es excluido
LastReturnFilter::LastReturnFilter(  ) : liblas::FilterI(eInclusion) {}


bool LastReturnFilter::filter(const liblas::Point& p)//implementación del metodo que evalua la condición
{

    // If the GetReturnNumber equals the GetNumberOfReturns,
    // we're a last return

    bool output = false;
    if (p.GetReturnNumber() == p.GetNumberOfReturns())
    {
        output = true;
    }

    // If the type is switched to eExclusion, we'll throw out all last returns.
    if (GetType() == eExclusion && output == true)
    {
        output = false;
    } else {
        output = true;
    }
    return output;
}


//AngleFilter
class AngleFilter: public liblas::FilterI
{
public:
    AngleFilter(float minAngle,float maxAngle);
    bool filter(const liblas::Point& point);

private:
    float mMinAngle,mMaxAngle;
    AngleFilter(AngleFilter const& other);
    AngleFilter& operator=(AngleFilter const& rhs);
};

AngleFilter::AngleFilter(float minAngle,float maxAngle) : liblas::FilterI(eInclusion),mMinAngle(minAngle),mMaxAngle(maxAngle) {}

bool AngleFilter::filter(const liblas::Point& p)
{
    bool output = false;
    if (p.GetScanAngleRank()>=mMinAngle &&  p.GetScanAngleRank()<=mMaxAngle) //Condición angular
    {
        output = true;
    }

    return output;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),mPCDCloud(new pcl::PointCloud<pcl::PointXYZ>()),
    mUpsamplingCloud(new pcl::PointCloud<pcl::PointXYZ>()),
    mLasCloud(new pcl::PointCloud<PointMMS>()),
    mTrajCloud(new pcl::PointCloud<PointTraj>())

{
    //Construcción de la interface gráfica
    ui->setupUi(this);

    setWindowTitle("SeminarioPCL_12_9_16");
    mPbLoadPCDFile = new QPushButton("Load PCD Cloud");
    mPbComputeUpsampling = new QPushButton("Compute Upsampling+Poisson");
    mPbLoadLasCloud = new QPushButton("Load Las Cloud");
    mPbLoadLasTraj = new QPushButton("Load Las Trajectory");
    mPbComputeDist2TrajFilter = new QPushButton("Compute Filter");
    mTabMainWidget = new QTabWidget();
    QWidget *widUpsampling = new QWidget();
    QVBoxLayout *layUpsampling = new QVBoxLayout();
    layUpsampling->addWidget(mPbLoadPCDFile);
    layUpsampling->addWidget(mPbComputeUpsampling);
    widUpsampling->setLayout(layUpsampling);

    mTabMainWidget->addTab(widUpsampling,"PCD Voxel Grid");

    QWidget *widDist2Traj = new QWidget();
    QVBoxLayout *layDist2Traj = new QVBoxLayout();
    layDist2Traj->addWidget(mPbLoadLasCloud);
    layDist2Traj->addWidget(mPbLoadLasTraj);
    layDist2Traj->addWidget(mPbComputeDist2TrajFilter);
    widDist2Traj->setLayout(layDist2Traj);
    mTabMainWidget->addTab(widDist2Traj,"Las Dist2Traj");

    setCentralWidget(mTabMainWidget);

    //Conexión de las señales "clicked(bool)" de cada botón con el slot correspondiente.
    connect(mPbLoadPCDFile,SIGNAL(clicked(bool)),this,SLOT(on_pbLoadPCDFile_Trigered()));
    connect(mPbLoadLasCloud,SIGNAL(clicked(bool)),this,SLOT(on_pbLoadLasCLoud_Trigered()));
    connect(mPbLoadLasTraj,SIGNAL(clicked(bool)),this,SLOT(on_pbLoadLasTraj_Trigered()));
    connect(mPbComputeUpsampling,SIGNAL(clicked(bool)),this,SLOT(on_pbComputeUpsampling_Trigered()));
    connect(mPbComputeDist2TrajFilter,SIGNAL(clicked(bool)),this,SLOT(on_pbComputeDist2TrajFilter_Trigered()));


}

MainWindow::~MainWindow() //Destructor por defecto
{
    delete ui;
}

void MainWindow::on_pbLoadPCDFile_Trigered(){

    //Establecemos un filtro segun la extensión del archivo para el dialogo de selección
    QString filters("PCD Files (*.PCD)");
    QString defaultFilter ="PCD Files (*.PCD)";

    //Abrimos dialogo de seleccion y guardamos la ruta del elemento seleccionado en "selectedFile"
    QString selectedFile = QFileDialog::getOpenFileName(this, tr("Open PCD File"), "C://",filters,&defaultFilter);
    if (!selectedFile.isEmpty()) {
        //Creamos un objeto QFileInfo que nos permite descomponer la ruta del archivo seleccionado en partes
        //(ruta completa,carpeta contenedora, nombre, extensión...) de forma sencilla e independientemente de la plataforma seleccionada.
        //Evitamos errores producidos por los elementos "/" o "\" dependientes del sistema operativo.
        mPcdFileInfo = new QFileInfo(selectedFile);
        mPCDCloud->clear();
        //Cargamos el archivo pcd en un objeto del tipo "PointCloud" mediante el método "pcl::io::loadPCDFile<PointType>(...)"
        if (pcl::io::loadPCDFile<pcl::PointXYZ> (selectedFile.toStdString(), *mPCDCloud) == -1) //* load the file
        {
          PCL_ERROR ("Couldn't read pcd file\n");
          return;
        }
    }
}

void MainWindow::on_pbLoadLasCLoud_Trigered(){

    //Establecemos un filtro segun la extensión del archivo para el dialogo de selección
    QString filters("Las Files (*.las)");
    QString defaultFilter ="Las Files (*.las)";

    //Abrimos dialogo de seleccion y guardamos la ruta del elemento seleccionado en "selectedFile"
    QString selectedFile = QFileDialog::getOpenFileName(this, tr("Open Cloud File"), "C://",filters,&defaultFilter);
    if (!selectedFile.isEmpty()) {
        //Creamos un objeto QFileInfo que nos permite descomponer la ruta del archivo seleccionado en partes
        //(ruta completa,carpeta contenedora, nombre, extensión...) de forma sencilla e independientemente de la plataforma seleccionada.
        //Evitamos errores producidos por los elementos "/" o "\" dependientes del sistema operativo.
        mLasFileInfo = new QFileInfo(selectedFile);

        ////Iniciamos la lectura del fichero Las
        //Creamos e iniciamos un "stream" o flujo de datos de entrada.
        std::ifstream ifs;
        ifs.open(selectedFile.toStdString(), std::ios::in | std::ios::binary);

        //Creamos e iniciamos un objeto de la clase "liblas::Reader" para la lectura del archivo las binario
        liblas::ReaderFactory f;
        liblas::Reader reader = f.CreateWithStream(ifs);

        //Añadimos los filtros al objeto de lectura "liblas::Reader"
        std::vector<liblas::FilterPtr> filters;

        liblas::FilterPtr return_filter = liblas::FilterPtr(new LastReturnFilter());
        filters.push_back(return_filter);

        float minAngle = -25.0f;
        float maxAngle = 90.0f;
        liblas::FilterPtr angle_filter = liblas::FilterPtr(new AngleFilter(minAngle,maxAngle));
        filters.push_back(angle_filter);

        reader.SetFilters(filters);

        //Leemos el primer punto del fichero para conocer la translacion en X e Y a aplicar debido al tamaño del "float"
        reader.ReadNextPoint();
        liblas::Point const& p =reader.GetPoint();
        mTranslationX = ((int)p.GetX()/10000)*10000;
        mTranslationY = ((int)p.GetY()/10000)*10000;

        //Creamos un  punto PCL con los valores leidos y lo añadimos al objeto "PointCloud" aplicando la traslación
        PointMMS point;
        point.x =p.GetX()-mTranslationX;
        point.y =p.GetY()-mTranslationY;
        point.z =p.GetZ();
        point.intensity = p.GetIntensity();
        point.angle = p.GetScanAngleRank();
        point.timeStamp = p.GetTime();
        mLasCloud->push_back(point);

        //Leemos el resto de puntos del fichero y los añadimos al objeto "PointCloud" aplicando la traslación
        while (reader.ReadNextPoint())
        {
            liblas::Point const& p = reader.GetPoint();
            point.x =p.GetX()-mTranslationX;
            point.y =p.GetY()-mTranslationY;
            point.z =p.GetZ();
            point.intensity = p.GetIntensity();
            point.angle = p.GetScanAngleRank();
            point.timeStamp = p.GetTime();
            mLasCloud->push_back(point);
        }

        //Cerramos el "stream" o flujo de datos de lectura.
        ifs.close();

    }
}

void MainWindow::on_pbLoadLasTraj_Trigered(){
    //Establecemos un filtro segun la extensión del archivo para el dialogo de selección
    QString filters("ASCII Files (*.txt)");
    QString defaultFilter ="ASCII Files (*.txt)";

    //Abrimos dialogo de seleccion y guardamos la ruta del elemento seleccionado en "selectedFile"
    QString selectedFile = QFileDialog::getOpenFileName(this, tr("Open Data File"), "C://",filters,&defaultFilter);
    if (!selectedFile.isEmpty()) {
        //Limpiamos el contenido actual de la nube de puntos donde almacenaremos la información
        mTrajCloud->clear();
        //Creamos un objeto QFileInfo que nos permite descomponer la ruta del archivo seleccionado en partes
        //(ruta completa,carpeta contenedora, nombre, extensión...) de forma sencilla e independientemente de la plataforma seleccionada.
        //Evitamos errores producidos por los elementos "/" o "\" dependientes del sistema operativo.
        QFileInfo *trajectoryFileInfo = new QFileInfo(selectedFile);

        ////Iniciamos la lectura del fichero Las
        //Creamos un objeto "QFile" que recoje la información necesaria para el trabajo con cualquier archivo almacenado en el disco duro.
        QFile file(trajectoryFileInfo->absoluteFilePath());
        if(!file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(0, "error", file.errorString());
        }

        //Creamos e iniciamos un "stream" o flujo de datos de entrada para archivos ASCII (texto plano).
        QTextStream in(&file);
        //Creamos e inicializamos una serie de variables auxiliares para el control de los elementos leidos y almacenados
        int id=1; //ID del punto para su identificación inequivoca.
        int lineNum=0; //Numero de linea para omitir la lectura de la cabecera del documento ASCII (24 lineas)
        double meanTrajZ =0; //Variable para el calculo de la altura media para tratar de alinear alturas ortométricas y alturas geoidales

        //Iteramos la lectura linea a linea hasta llegar al final del documento.
        while(!in.atEnd()) {
            lineNum++; //Incrementamos el numero de linea -> lineNum++ => lineNum+=1 => lineNum =lineNum+1
            QString line = in.readLine(); //Leemos la siguiente linea
            if (lineNum>24) { //Saltamos las primeras 24 lineas del archivo.
                //Segmentamos la linea en elementos diferentes segun la posición de los separadores definidos en QRegExp (tabulador y espacio)
                QRegExp sep("(\t| )");
                QStringList fields = line.split(sep,QString::SkipEmptyParts);
                //Creamos un objeto de la clase "PointTraj" diseñada y definida según nuestras necesidades y rellenamos los campos con la información adecuada
                PointTraj pTraj;
                pTraj.timeStamp=fields[0].toDouble();
                pTraj.odometerLecture=fields[1].toDouble();
                pTraj.x=(float)(fields[2].toDouble()-mTranslationX);
                pTraj.y=(float)(fields[3].toDouble()-mTranslationY);
                pTraj.z=fields[4].toFloat();
                pTraj.roll=fields[8].toFloat();
                pTraj.pitch=fields[9].toFloat();
                pTraj.yaw=fields[10].toFloat();
                mTrajCloud->push_back(pTraj); //Añadimos el punto creado a la nube de puntos (PointCloud) que representa la trayectoria
                meanTrajZ+=pTraj.z;//Incrementamos el valor acumulado de Z para el calculo de la media
                id++;//incrementamos el identificador único
            }

        }
        //Cerramos el flujo de entrada.
        file.close();

        //aproximo la trayectoria a la altura de la nube para evitar diferencia entre altitudes sobre geoide y ortométricas
        meanTrajZ = meanTrajZ/mTrajCloud->points.size();
        double meanCloudZ =0;

        //Calulamos el valor medio de Z de la nube de puntos. Optimización con BOOST_FOREACH
        BOOST_FOREACH(PointMMS& point, mLasCloud->points){
            meanCloudZ+=point.z;
        }

        meanCloudZ =meanCloudZ/mLasCloud->points.size();

        //Aplicamos el offset en cota a los puntos de la trayectoria iterando sobre toda la nube de puntos.
        float zDif = meanTrajZ-meanCloudZ;
        for (int i = 0; i < mTrajCloud->points.size(); ++i) {
            mTrajCloud->points[i].z = mTrajCloud->points[i].z - zDif;
        }
    }
}

void MainWindow::on_pbComputeUpsampling_Trigered(){
     ////Upsampling mediante metodos de interpolación
     //Creamos una instancia (objeto) de la clase "MovingLeastSquares"
     pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ> mls;
     mls.setInputCloud (mPCDCloud);//Especificamos la nube a procesear
     mls.setSearchRadius (0.02);//Establecemos los parámetros de procesamiento propios del algoritmo
     mls.setPolynomialFit (true);
     mls.setPolynomialOrder (3);
     mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ>::SAMPLE_LOCAL_PLANE);
     mls.setUpsamplingRadius (0.005);
     mls.setUpsamplingStepSize (0.003);
     //Creamos el objeto "PointCloud" que recojerá los resultados
     pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_Upsampled (new pcl::PointCloud<pcl::PointXYZ> ());
     mls.process (*cloud_Upsampled); //Iniciamos el proceso

     ////Calculo de las normales en cada punto de forma asincrona (paralelo o multicore) con OpenMP
     //Creamos una instancia (objeto) de la clase "NormalEstimationOMP"
     pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> ne;
     ne.setInputCloud (cloud_Upsampled);//Especificamos la nube a procesear
     ne.setNumberOfThreads (8);//Establecemos los parámetros de procesamiento propios del algoritmo
     //Podríamos identificar el numero maximo de hilos que la CPU es capaz de ejecutar en paralelo utilizando Boost
//     unsigned nThreads = boost::thread::hardware_concurrency();
//     ne.setNumberOfThreads (nThreads);
     ne.setRadiusSearch (0.01);

     //Calculamos el centroide para orientar las normales calculadas
     Eigen::Vector4f centroid;
     pcl::compute3DCentroid (*cloud_Upsampled, centroid);
     ne.setViewPoint (centroid[0], centroid[1], centroid[2]);
     //Creamos el objeto "PointCloud" que recojerá los resultados
     pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal> ());
     ne.compute (*cloud_normals);//Iniciamos el proceso
     //Giramos las normales para que apunten hacia el exterior de la superficie
     for (size_t i = 0; i < cloud_normals->size (); ++i)
     {
     cloud_normals->points[i].normal_x *= -1;
     cloud_normals->points[i].normal_y *= -1;
     cloud_normals->points[i].normal_z *= -1;
     }
     //Creamos el objeto "PointCloud" que recojerá los resultados con coordenadas XYZ y las componentes de la normal
     pcl::PointCloud<pcl::PointNormal>::Ptr cloud_Upsampled_normals (new pcl::PointCloud<pcl::PointNormal> ());
     pcl::concatenateFields (*cloud_Upsampled, *cloud_normals, *cloud_Upsampled_normals);
     //Guardamos el rersultado en un archivo Pcd
     pcl::io::savePCDFileBinary(mPcdFileInfo->absoluteDir().absoluteFilePath(mPcdFileInfo->baseName()+"_upsampled.pcd").toStdString(), *cloud_Upsampled_normals);

     ////Calculo de la malla mediante un algoritmo Poisson
     //Creamos una instancia (objeto) de la clase "Poisson"
     pcl::Poisson<pcl::PointNormal> poisson;
     poisson.setInputCloud(cloud_Upsampled_normals); //Especificamos la nube a procesear
     poisson.setDepth (9);//Establecemos los parámetros de procesamiento propios del algoritmo
     pcl::PolygonMesh mesh;     //Creamos el objeto "PolygonMesh" que recojerá los resultados
     poisson.reconstruct (mesh);//Iniciamos el proceso
     //Guardamos el rersultado en un archivo Ply
     io::savePLYFile(mPcdFileInfo->absoluteDir().absoluteFilePath(mPcdFileInfo->baseName()+"_mesh.ply").toStdString(), mesh);
}

void MainWindow::on_pbComputeDist2TrajFilter_Trigered(){

    ////Busqueda de puntos cercanos por kd-Tree
    //Creamos el objeto "PointCloud" que recojerá los resultados
    pcl::PointCloud<PointMMS>::Ptr filteredCloud(new pcl::PointCloud<PointMMS>());
    //Creamos el objeto "KdTreeFLANN" que permite la organización de una nube de puntos no estructurada y la busqueda de puntos cercanos
    //de forma eficiente mediante estrategias ANN (Approximate Nearest Neighbors)
    pcl::KdTreeFLANN<PointTraj> kdtree;

    kdtree.setInputCloud (mTrajCloud);//Especificamos la nube a organizar
    int K = 1; //Especificamos que queremos encontrar unicamente el punto más cercano con K=1

    //Realizamos un proceso iterativo de busqueda del punto mas cercano de la trayectoria para cada punto de la nube
    BOOST_FOREACH(PointMMS& point, mLasCloud->points){
        std::vector<int> pointIdxNKNSearch(K);
        std::vector<float> pointNKNSquaredDistance(K);
        PointTraj searchPoint;
        searchPoint.x = point.x;
        searchPoint.y = point.y;
        searchPoint.z = point.z;
        float dist2Traj = 10.0f;
        kdtree.nearestKSearch (searchPoint, K, pointIdxNKNSearch, pointNKNSquaredDistance);
        //Si la distancia entre el punto a evaluar y el punto mas cercano es menor que el radio definido añadimos el punto
        //al objeto "PointCloud" que recoge el resultado
        if ( pointIdxNKNSearch.size() > 0  && std::sqrt(pointNKNSquaredDistance[0])<=dist2Traj)
        {
            filteredCloud->points.push_back(point);
        }
    }


    ////Guardamos el resultado en formato Las
    //Leemos la cabecera del archivo de entrada para mantener sus parámetros
    std::ifstream ifs;
    ifs.open(mLasFileInfo->absoluteFilePath().toStdString(), std::ios::in | std::ios::binary);
    liblas::ReaderFactory f;
    liblas::Reader reader = f.CreateWithStream(ifs);
    //Creamos un archivo y un stream de salida donde guardar los datos
    std::ofstream ofs(mLasFileInfo->absoluteDir().absoluteFilePath(mLasFileInfo->baseName()+"_filtered.las").toStdString(),std::ios::out | std::ios::binary);
    liblas::Writer writer(ofs, reader.GetHeader());

    //Iteramos sobre la nube de puntos que contiene el resultado
    for (int i = 0; i < filteredCloud->points.size(); ++i) {
        //Creamos un punto Las
        liblas::Point p(&reader.GetHeader());
        //Rellenamos los campos con los valores del punto a guardar
        p.SetX((double)filteredCloud->points.at(i).x+mTranslationX);
        p.SetY((double)filteredCloud->points.at(i).y+mTranslationY);
        p.SetZ(filteredCloud->points.at(i).z);
        p.SetIntensity(filteredCloud->points.at(i).intensity);
        p.SetTime(filteredCloud->points.at(i).timeStamp);
        p.SetScanAngleRank(filteredCloud->points.at(i).angle);
        //Escribimos el punto en el fichero
        writer.WritePoint(p);
    }
    //Cerramos el "stream" de salida
    ofs.close();
}
