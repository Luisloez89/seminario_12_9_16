#-------------------------------------------------
#
# Project created by QtCreator 2016-09-06T13:14:28
#
#-------------------------------------------------
# ensure one "debug" or "release" in CONFIG so they can be used as
# conditionals instead of writing "CONFIG(debug, debug|release)"...
debug_and_release {
    CONFIG -= debug_and_release
    CONFIG += debug_and_release
}
CONFIG(debug, debug|release) {
    CONFIG -= debug release
    CONFIG += debug
    }

CONFIG(release, debug|release) {
        CONFIG -= debug release
        CONFIG += release
}

#Añadimos las funcionales de Qt a utilizar (Nucleo y elementos de interface gráfica GUI)
QT       += core gui

#Nos aseguramos de que la versión de Qt utilizada en el kit de desarrollo sea superior a la 4.0
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#Nombre de la aplicación
TARGET = SeminarioPCL_12_9_16
TEMPLATE = app

#Archivos de implementación de clases
SOURCES += main.cpp\
        MainWindow.cpp

#Archivos de definición de clases
HEADERS  += MainWindow.h \
    PointDefinitions.h

#Archivos de diseño de formularios (GUI)
FORMS    += MainWindow.ui

#Ruta en relativo de la carpeta "include" de las librerías a utilizar
INCLUDEPATH += ../../Libs/PCL_1.8.0_Vs10_x64/include/pcl-1.8
INCLUDEPATH += ../../Libs/boost_1_61_0_vs10_x64/include
INCLUDEPATH += ../../Libs/Eigen_vs10_x64/include
INCLUDEPATH += ../../Libs/libLAS/libLAS-1.8.0_x64/include
INCLUDEPATH += ../../Libs/flann_vs10_x64/include

#Rutas en relativo da carpeta que contiene los binarios de las librerías a utilizar. La ruta de esta carpeta deberá ir precedida de "-L"
#Al trabajar con rutas relativas es necesario incluir "$$PWD/" antes del inicio de la ruta para que el compilador no incluya "-L" como
#un componente de la misma.
LIBS += -L$$PWD/../../Libs/PCL_1.8.0_Vs10_x64/bin
LIBS += -L$$PWD/../../Libs/PCL_1.8.0_Vs10_x64/lib
LIBS += -L$$PWD/../../Libs/boost_1_61_0_vs10_x64/bin

debug{
      #Directriz de compilador necesaria cuando utilizemos tipos de puntos implementados por nosotros
      QMAKE_CXXFLAGS += -bigobj

      LIBS += -L$$PWD/../../Libs/libLAS/libLAS-1.8.0_x64/bin/Debug

      #Nombre de los archivos binarios de las librerías a utilizar. La ruta de esta carpeta deberá ir precedida de "-l"
      LIBS += -lpcl_common_debug -lpcl_surface_debug -lpcl_features_debug -lpcl_filters_debug -lpcl_io_debug -lpcl_kdtree_debug -lpcl_octree_debug -lpcl_sample_consensus_debug -lpcl_search_debug -lpcl_segmentation_debug
}else{
      LIBS += -L$$PWD/../../Libs/libLAS/libLAS-1.8.0_x64/bin/Release

      LIBS += -lpcl_common_release -lpcl_surface_release -lpcl_features_release -lpcl_filters_release -lpcl_io_release -lpcl_kdtree_release -lpcl_octree_release -lpcl_sample_consensus_release -lpcl_search_release -lpcl_segmentation_release
}

      LIBS += -lliblas
